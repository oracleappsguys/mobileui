var app = angular.module("userApp", []);
app.controller("userCtrl", function($scope,$window) {
  $scope.currentPage = 1;
  $scope.numPerPage = 5;
  $scope.totPage = 1;
  $scope.maxSize;
  $scope.inscnt=0;
  $scope.updcnt=0;
  $scope.datasetsfiltered;
  $scope.transformeddata=[];
  $scope.orderByMe = function(x) {
		console.log("Sort "+x);
        $scope.myOrderBy = x;
	  }
$scope.changestatus=function(row)
{
	console.log("in function");
	if (row.status==0){
		row.status=1;
	}	
}
$scope.insertrow=function()
{
	var datarow={}
	datarow.username=null;
	datarow.password=null;
	datarow.createdby=null;
	datarow.creationdate=null;
	datarow.last_updated_by=null;
	datarow.last_update_date=null;		
	datarow.designation=null;
	datarow.phone_number=null;
	datarow.email_address=null;
	datarow.role=null;
	datarow.status=-1;
	datarow.deletecb=false;
	datarow.message=null;
	//$scope.transformeddata[$scope.transformeddata.length]=datarow;
	//$scope.transformeddata.unshift(datarow);
	$scope.transformeddata.splice((($scope.currentPage-1)*$scope.numPerPage),0,datarow);
	$scope.getcurrentpage($scope.currentPage);
}

$scope.deleterow=function (row,index)
{
	var delxhr = new XMLHttpRequest();
	delxhr.withCredentials = true;
	console.log("in Delete");
	delxhr.addEventListener("readystatechange", function () {
		  if (this.readyState === 4) {				 
			  console.log("this.responseText"+this.responseText);
			  var opresponse=JSON.parse(this.responseText);
				 console.log("this.responseText : "+opresponse.data[0][0]);
				 if(opresponse.data[0][0]!=0)
				 {
					 console.log("record deleted");				
				 }
				 //PAGE 3  filteredset 1 -- 11 ,2-12,0-10
				 console.log("deleting record["+(index+(($scope.currentPage-1)*$scope.numPerPage))+"] from transformedindex.");
				 console.log("Record to be deleted : ");
				 alert("Record has been deleted successfuly");
				 console.log($scope.transformeddata[(index+(($scope.currentPage-1)*$scope.numPerPage))]);
				 $scope.transformeddata.splice((index+(($scope.currentPage-1)*$scope.numPerPage)),1);
				 //window.location.reload();
				 $scope.getcurrentpage($scope.currentPage);
			  }
	});
	delxhr.open("POST", "http://localhost:8080/MobilizeDemo/CRUDServlet",false);
	delxhr.setRequestHeader("context", "Jndj7G8NyObPgCaa6JjD/Z/g761S8fW4JmDaxzkMQwHqPGGldEJLijfaj5Xw4fL3");
	delxhr.setRequestHeader("usrparm", JSON.stringify({1 : row.username}));
	delxhr.send(null);

}
	
$scope.updatedata=function (){
	console.log("Update button clicked...");
	$scope.inscnt=0;
	$scope.updcnt=0;
	console.clear();
	for (i in $scope.transformeddata)
	{
		var row=$scope.transformeddata[i];
		console.log("row["+i+"] is " + row.deletecb + " ,Status is "+ row.status);		
		//console.log(JSON.stringify({1 : row.password,2 : row.designation, 3: row.phone_number, 4 : row.email_address, 5 : row.role , 6 : row.last_updated_by , 7 : row.last_update_date, 8 : row.username}));
		var savexhr = new XMLHttpRequest();
		savexhr.withCredentials = true;
			
		//console.log("checking record for current row : "+ $scope.transformeddata[row].status +" with username : "+$scope.transformeddata[row].username);
			console.log("in Update");
			if(row.status!=0 )
			{
			  if (row.status==1){
				  savexhr.addEventListener("readystatechange", function () {
						  if (this.readyState === 4) {				 
							  console.log("this.responseText"+this.responseText);
							  var opresponse=JSON.parse(this.responseText);
								if(opresponse.status=="ERROR"){
								  $scope.updcnt=$scope.updcnt+1;
								  row.deletecb=true;
								  row.message=opresponse.message.substr(0,opresponse.message.indexOf(":"));
							  }else{
								 if(opresponse.data[0][0]!=0)
								 {
									 row.deletecb=false;
									 row.message=null;
									 console.log("record _updated");
									 //$scope.$apply();  
								 }
							    } 
							  }
					});
				  savexhr.open("POST", "http://localhost:8080/MobilizeDemo/CRUDServlet",false);
				  savexhr.setRequestHeader("context", "Ep2uA/TiKQFs6uCXeMltx/RkBPpUV1dU/69i+HQPMib30fppeSXixiNIgsHgRAYTX7KQvLcoa0jK6y8mMV8zwuRKg1DT4TGQ9dIgfVQi+2lfUzJx68KUE4eVGuqNmDyEx94+h79Dfncpwt3KAZlU4KflFZl20cvSH/QIlwl5hFuHkPkQuKm7f9cp4ksdShxlPzCyNlAopV6lvmHJFOo1sw==");
				  savexhr.setRequestHeader("usrparm", JSON.stringify({1 : row.password,2 : row.designation, 3: row.phone_number, 4 : row.email_address, 5 : row.role , 6 : row.last_updated_by , 7 : row.last_update_date, 8 : row.username}));
				  savexhr.send(null);
			  }	
			  else if(row.status==-1){
				  savexhr.addEventListener("readystatechange", function () {
					  if (this.readyState === 4) {				 
						  console.log("this.responseText"+this.responseText);
						  var opresponse=JSON.parse(this.responseText);
						
						  if(opresponse.status=="ERROR"){
							  $scope.inscnt=$scope.inscnt+1;
							  row.deletecb=true;
							  row.message=opresponse.message.substr(0,opresponse.message.indexOf(":"));
							  row.status=-1;
						  }
						  else{	  
							 if(opresponse.data[0][0]!=0)
							 {
								 row.deletecb=false;
								 row.message=null;
								 console.log("record Inserted");
								 row.status=0;
							 }
					        }
						  }
				});
				  savexhr.open("POST", "http://localhost:8080/MobilizeDemo/CRUDServlet",false);
				  savexhr.setRequestHeader("context", "yVXxpv8sSdktJREm7uILNJ/g761S8fW4gT+tCSUkj1gmylmem0qWGzISiomAeATXbJJGJgjduwfP49SZkZ0DlnqnGQLbYnl29dIgfVQi+2lKl9+tGDNZ+QFKt6uuIyBCgrPjozf6O7t+LndPaagznTKdJv1HNef1H/QIlwl5hFtR4wXvASWggR/0CJcJeYRb7uzagvm7nDj1WAXkbFpJ31Co8FKsvDYWEXJH4Hln3KD/PMmKnS0zxHqsiLo7GwgNkhFl77Rj7KcXkyKeVaWjJ+quUcSD+avA");
				  savexhr.setRequestHeader("usrparm", JSON.stringify({1 : row.username,2 : row.password,3 : row.designation, 4: row.phone_number, 5 : row.email_address, 6 : row.role , 7 : row.createdby, 8: row.last_updated_by}));
				  savexhr.send(null);
				
			  }
			}	
	}
	
	$scope.getcurrentpage($scope.currentPage);
	if($scope.inscnt+$scope.updcnt>0)
	$scope.ShowAlert($scope.inscnt+$scope.updcnt+" record is in error status");
	else
		$scope.ShowAlert("Record is updated/inserted successfully");
};
$scope.ShowAlert = function (message) {
    
    $window.alert(message);
};	
var data=null;
var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

$scope.getnextpage=function()
{
$scope.currentPage=$scope.currentPage+1;
$scope.getcurrentpage($scope.currentPage);
}
$scope.getprevpage=function()
{
$scope.currentPage=$scope.currentPage-1;
$scope.getcurrentpage($scope.currentPage);
}
$scope.getcurrentpage=function(v_current_page){
var firstindex =($scope.numPerPage * (v_current_page-1));
var lastindex =($scope.numPerPage * v_current_page);
console.log( " for "+v_current_page+" page , first index :"+firstindex+" , lastindex : "+lastindex);
//console.log("transformeddata is : ");
//console.log($scope.transformeddata);
$scope.datasetsfiltered = $scope.transformeddata.slice(firstindex,lastindex);
//console.log("datasetsfiltered is : ");
//console.log($scope.datasetsfiltered);
//console.log("datasetsfiltered.length = " + $scope.datasetsfiltered.length);
if($scope.datasetsfiltered.length==0)	
{
	$scope.currentPage=$scope.currentPage-1;
	$scope.getcurrentpage($scope.currentPage);
}
else
{
	adjustpagination();
}
}  

function adjustpagination()
{	
$scope.totPage=Math.ceil($scope.transformeddata.length/$scope.numPerPage);
if($scope.currentPage==1) document.getElementById("prevpagebtn").disabled = true;
else document.getElementById("prevpagebtn").disabled = false;
if($scope.currentPage==Math.ceil($scope.transformeddata.length/$scope.numPerPage)) 
document.getElementById("nextpagebtn").disabled = true;
else document.getElementById("nextpagebtn").disabled = false;
}


xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {
	 //console.log("http response call back invoked");
	 //console.log("this.responseText : "+this.responseText);
    $scope.response = JSON.parse(this.responseText);
	$scope.data=$scope.response.data;
	for (var i=0;i<$scope.data.length;i++)
	{
		var datarow={};
		datarow.status=0;
		datarow.message="";
		datarow.deletecb=false;
		datarow.username=$scope.data[i][0];
		datarow.password=$scope.data[i][1];
		datarow.createdby=$scope.data[i][2];
		datarow.creationdate=$scope.data[i][3];
		datarow.last_updated_by=$scope.data[i][4];
		datarow.last_update_date=$scope.data[i][5];		
		datarow.designation=$scope.data[i][6];
		datarow.phone_number=$scope.data[i][7];
		datarow.email_address=$scope.data[i][8];
		datarow.role=$scope.data[i][9];
		$scope.transformeddata[i]=datarow;
	}
	//console.log(JSON.stringify($scope.transformeddata));
	$scope.header=$scope.response.header;
	//$scope.$apply();
	$scope.maxSize = $scope.transformeddata.length;
	$scope.getcurrentpage($scope.currentPage);
	adjustpagination();
  }
});
console.log("Initiating http request");
xhr.open("POST", "http://localhost:8080/MobilizeDemo/DataListener",false);
xhr.setRequestHeader("context", "5dt8dWzcHGxZfOdyGUOAtK7WuNnkroX/hdOKNOHMZgg=");
xhr.setRequestHeader("usrparm", "{}");
xhr.send(data);
});

